package twitch.angelandroidapps.angelstylisedimages;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;

import twitch.angelandroidapps.ads.ApplicationAdViewAdMob3;
import twitch.angelandroidapps.angelstylisedimages.domain.entities.MediaFile;
import twitch.angelandroidapps.angelstylisedimages.presentation.dialogs.ShareImageDialog;

public class MainActivity extends AppCompatActivity
        implements MainActivityFragment.IViewManagerTensor.IEventCallback {

    private static final String TAG = "Angel: Main A";

    AppBarLayout appBarLayout;
    Toolbar toolbar;
    ViewGroup adView;

    private MenuItem menuItemSaveImage;
    private MenuItem menuItemShareImage;

    private MainActivityFragment fragmentMain;


    //select from gallery
    private ActivityResultLauncher<String> selectImageLauncher;
    //take camera and permission if needed
    private Uri imageUri;
    private ActivityResultLauncher<String> requestCameraPermissionLauncher;
    private ActivityResultLauncher<Uri> takePictureLauncher;
    //same image permission.
    private ActivityResultLauncher<String[]> requestReadWritePermissionLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stylised_image);

        appBarLayout = findViewById(R.id.app_bar_layout);
        toolbar = findViewById(R.id.toolbar);
        adView = findViewById(R.id.ad_view);

        //update menu
        setupToolbar();

        fragmentMain = (MainActivityFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment);

        ApplicationAdViewAdMob3 appAd = new ApplicationAdViewAdMob3(this, null);
        appAd.loadAdaptiveAd(adView, R.string.admob_ad_unit_id_banner);

        //launchers
        selectImageLauncher = registerForActivityResult(
                new ActivityResultContracts.GetContent(),
                uri -> fragmentMain.loadImageIfNeeded(uri)
        );
        requestCameraPermissionLauncher = registerForActivityResult(
                new ActivityResultContracts.RequestPermission(),
                isGranted -> {
                    if (isGranted) {
                        launchCamera();
                    } else {
                        doSnackBar("function requires Camera permission");
                    }
                }
        );
        takePictureLauncher = registerForActivityResult(
                new ActivityResultContracts.TakePicture(),
                isSuccess -> {
                    if (isSuccess) {
                        fragmentMain.loadImageIfNeeded(imageUri, "time:" + System.currentTimeMillis());
                    }
                });

        requestReadWritePermissionLauncher = registerForActivityResult(
                new ActivityResultContracts.RequestMultiplePermissions(),
                permissions -> {
                    Boolean readPermissionGranted = permissions.get(android.Manifest.permission.READ_EXTERNAL_STORAGE);
                    Boolean writePermissionGranted = permissions.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

                    // Default to false if the permission is not in the map
                    if (readPermissionGranted == null) readPermissionGranted = false;
                    if (writePermissionGranted == null) writePermissionGranted = false;

                    if (readPermissionGranted && writePermissionGranted) {
                        // Both permissions granted, you can now launch the camera
                        saveImage();
                    } else {
                        // Handle the permission denial
                        doSnackBar("save image requires Write permission");
                    }
                }
        );
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        fragmentMain.loadImageIfNeeded(intent);

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);


        if (savedInstanceState == null) {
            /*Creating new activity*/
            Intent intent = getIntent();

            fragmentMain.loadImageIfNeeded(intent);
        }
    }


    //MENU RELATED
    //============
    private void setupToolbar() {

        if (toolbar != null) {
            //  toolbar.setTitle("");
            toolbar.setSubtitle("");
            setSupportActionBar(toolbar);

            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
//                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_stylised_image, menu);


        menuItemSaveImage = menu.findItem(R.id.menu_save_image);
        menuItemShareImage = menu.findItem(R.id.menu_share_image);

        menuItemShareImage.setVisible(false);
        menuItemSaveImage.setVisible(false);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int itemId = item.getItemId();
        if (itemId == R.id.menu_local_album) {
            prepareAlbum();
        } else if (itemId == R.id.menu_camera) {
            checkAndLaunchCamera();
        } else if (itemId == R.id.menu_share_image) {
            shareImage();
        } else if (itemId == R.id.menu_save_image) {
            checkAndSaveImage();
        }

        return super.onOptionsItemSelected(item);
    }

    private void showShareAndSaveMenu() {
        menuItemSaveImage.setVisible(true);
        menuItemShareImage.setVisible(true);
    }

    private void hideShareAndSaveMenu() {
        menuItemSaveImage.setVisible(false);
        menuItemShareImage.setVisible(false);
    }

    //SHARE AND SAVE IMAGE
    //=====================
    private void checkAndSaveImage() {

        if (hasReadWritePermission()) {
            saveImage();
        } else {
            requestReadWritePermissionLauncher.launch(new String[]{
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            });
        }
    }

    private boolean hasReadWritePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            // For Android 11 (SDK 30) and above, no need to check READ_EXTERNAL_STORAGE for gallery access
            return true;  // Always return true as scoped storage handles access
        } else {
            boolean readPermissionGranted = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED;
            boolean writePermissionGranted = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED;
            return readPermissionGranted && writePermissionGranted;
        }
    }

    private void saveImage() {
        saveImageOriginal();
    }

    private void saveImageOriginal() {

        MediaFile file = fragmentMain.saveExternalImage();
        if (file == null) {
            doSnackBar("Cannot save Image.");
        } else {
            addToGallery(file);
            doSnackBar("Saved image " + file.getName() + ".");
        }

    }

    private void shareImage() {

        File file = fragmentMain.saveInternalImg();
        if (file == null || !file.exists()) {
            doSnackBar("Error sharing image.");
        } else {
            ShareImageDialog.openDialog(this, file);
        }
    }

    @Override
    public void isImageAvailable(MainActivityFragment.IViewManagerTensor.ImageState isImageAvailable) {

        switch (isImageAvailable) {
            case NoImage -> {
                hideShareAndSaveMenu();
            }
            case CreatingImage -> {
                hideShareAndSaveMenu();
            }
            case ImageAvailable -> {
                showShareAndSaveMenu();
            }
        }
        fragmentMain.setGridAndQualityVisibility(isImageAvailable);
    }

    //NAVIGATION DRAWER RELATED
    //=========================

    private void addToGallery(MediaFile f) {

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);

        mediaScanIntent.setData(f.getUri());
        this.sendBroadcast(mediaScanIntent);
    }

    //PERMISSION RELATED
    //==================
    @SuppressWarnings("EmptyMethod")
    private void updateDrawerSettingsPermission() {
        //TODO PERMISSION CHANGED (UPDATE DRAWER)
    }


    //CAMERA AND IMAGE RELATED
    //========================


    private void prepareAlbum() {

        selectImageLauncher.launch("image/*");
    }


    private void processError(String prefix, Throwable error) {
        doSnackBar(prefix + error.getLocalizedMessage());
    }


    private void checkAndLaunchCamera() {
        if (hasCameraPermission()) {
            launchCamera();
        } else {
            requestCameraPermissionLauncher.launch(android.Manifest.permission.CAMERA);
        }
    }

    private boolean hasCameraPermission() {
        return ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED;
    }

    private void launchCamera() {
        File photoFile = getOrCreateImageFile();
        imageUri = getContentUriForFile(photoFile);
        takePictureLauncher.launch(imageUri);
    }

    private File getOrCreateImageFile() {
        File storageDir = getExternalFilesDir(null);
        String fileName = "camera_image.jpg"; // Use a static name if reusing the same file
        File file = new File(storageDir, fileName);

        // Check if the file exists, otherwise create a new one
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                processError("Camera error", e);
            }
        }

        return file;
    }

    private Uri getContentUriForFile(File file) {
        return FileProvider.getUriForFile(this, "twitch.angelandroidapps.tracerTF", file);
    }

    //SNACK BAR
    //=========
    private void doSnackBar(String s) {
        toolbar.post(() -> Snackbar.make(toolbar, s.trim(), Snackbar.LENGTH_SHORT).show());

    }

    @SuppressWarnings("unused")
    private void print(String s) {
        Log.d(TAG, s);
    }

}

package twitch.angelandroidapps.angelstylisedimages.domain.handlers;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

/**
 * Created by Angel on 25/9/2017.
 */

public class PreferenceHandler {

    //BASE shared preference
    private static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private static SharedPreferences.Editor getEditor(Context context) {

        return getPreferences(context).edit();
    }


    //TENSOR FLOW SETTINGS
    //====================
    public static int getQualityIndex(Context context) {
        return getPreferences(context).getInt("quality_index", 0);
    }

    public static void setQualityIndex(Context context, int value) {
        getEditor(context).putInt("quality_index", value).commit();
    }

    public static float[] getStyleVals(Context context, float[] defValues) {
        String val = getPreferences(context).getString("style_vals", "");

        if (!val.isEmpty()) {
            String[] parts = val.split(",");
            if (parts.length == defValues.length) {
                float[] vals = new float[parts.length];
                for (int i = 0; i < parts.length; ++i) {
                    vals[i] = Float.parseFloat(parts[i]);
                }

                return vals;
            }
        }

        return defValues;
    }

    public static void setStyleVals(Context context, float[] tokens) {

        getEditor(context).putString("style_vals", getStringVal(tokens)).commit();

    }

    private static String getStringVal(float[] values) {
        final String delimiter = ",";
        StringBuilder sb = new StringBuilder();
        boolean firstTime = true;
        for (Object token : values) {
            if (firstTime) {
                firstTime = false;
            } else {
                sb.append(delimiter);
            }
            sb.append(token);
        }
        return sb.toString();
    }

}

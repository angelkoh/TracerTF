package twitch.angelandroidapps.angelstylisedimages.domain.handlers

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import twitch.angelandroidapps.angelstylisedimages.R
import twitch.angelandroidapps.angelstylisedimages.domain.entities.MediaFile
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

// * Created by Angel on 12/27/2019 10:36 AM.
// * Originally created for project "ContinuousLineArt".
// * Copyright (c) 2019 Angel. All rights reserved. 

object FileHandler {

    private const val TAG = "Angel: File H"
    private fun print(s: String) = Log.d(TAG, s)

    private const val QUALITY: Int = 100
    private const val FOLDER_NAME = "TracerTF"
    private val sdf = SimpleDateFormat("yyMMdd_hhmmss", Locale.US)

    fun saveExternal(context: Context, bitmap: Bitmap, fileName: String = ""): MediaFile? {

        val fn = fileName.ifBlank { getFileName() }

        val uri =  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            saveExternalQ(context, bitmap, fn)
        } else {
            saveExternalLegacy(context, bitmap, fn)
        }

        return uri?.let { MediaFile(it, "$FOLDER_NAME/$fn") }

    }


    //EXTERNAL
    @RequiresApi(Build.VERSION_CODES.Q)
    private fun saveExternalQ(context: Context, bitmap: Bitmap, fileName: String): Uri? {

        val relativeLocation = Environment.DIRECTORY_PICTURES + "/" + FOLDER_NAME

        val contentValues = ContentValues()
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, relativeLocation)

        context.contentResolver.let { resolver ->
            val uri = resolver.insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                contentValues
            )

            uri?.let {

                try {
                    resolver.openOutputStream(it)!!.use { outStream ->
                        bitmap.compress(Bitmap.CompressFormat.PNG, QUALITY, outStream)
                    }
                } catch (e: Exception) {
                    print("Failed to save $fileName ($e)")
                    null
                }
            } ?: print("Cannot create file: $fileName ($relativeLocation)")
            return uri

        }

    }

    @Suppress("DEPRECATION")
    private fun saveExternalLegacy(context: Context, bitmap: Bitmap, fileName: String): Uri? {

        val imagesFolder = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            FOLDER_NAME
        ).also {
            if (!it.mkdirs()) {
                print("Directory not created")
            }
        }

        return try {
            val file = File(imagesFolder, fileName)
            file.outputStream().use { fos ->
                bitmap.compress(Bitmap.CompressFormat.PNG, QUALITY, fos)
            }
            val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
            mediaScanIntent.data = Uri.fromFile(file)
            context.sendBroadcast(mediaScanIntent)

            FileProvider.getUriForFile(
                context,
                context.getString(R.string.file_provider_authority),
                file
            )

        } catch (e: Exception) {
            print("Exception while trying to write file for sharing: " + e.message)
            null
        }

    }

    fun saveCache(context: Context, bitmap: Bitmap, fileName: String = "shared_image.png"): Uri? {

        //folder "images" tallies with file_provider_paths.xml under "path" tag
        val imagesFolder = File(context.cacheDir, "shared").also {
            it.mkdirs()
        }

        return try {
            val file = File(imagesFolder, fileName)
            file.outputStream().use { fos ->
                bitmap.compress(Bitmap.CompressFormat.PNG, QUALITY, fos)
            }
            FileProvider.getUriForFile(
                context,
                context.getString(R.string.file_provider_authority),
                file
            )

        } catch (e: Exception) {
            print("Exception while trying to write file for sharing: " + e.message)
            null
        }
    }

    fun getFileName() = "img_${sdf.format(Date())}.jpg"
    fun getFolderName() = "${Environment.DIRECTORY_PICTURES}/$FOLDER_NAME"
}
package twitch.angelandroidapps.angelstylisedimages.domain.entities;

/**
 * Created by Angel on 21/9/2017.
 */

public class StyleType {

    private final String[] data;

    public StyleType(String text) {

        data = text.split("\\s*;\\s*");
    }

    public String getStyle() {
        return (data.length > 0) ? data[0].trim() : "0";
    }

    public String getArtist() {

        return (data.length > 1) ? data[1] : "";
    }

    private String getYear() {

        return (data.length > 2) ? data[2] : "";
    }

    private String getTitle() {

        return (data.length > 3) ? data[3] : "";
    }

    public String getTitleWithYear() {
        String year = getYear();
        String title = getTitle();
        return year.isEmpty() ? title : title + " (" + year + ")";
    }
}

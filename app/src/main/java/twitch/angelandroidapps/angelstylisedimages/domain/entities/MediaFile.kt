package twitch.angelandroidapps.angelstylisedimages.domain.entities

import android.net.Uri

data class MediaFile(
    val uri: Uri,
    val name: String
)
package twitch.angelandroidapps.angelstylisedimages;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.File;

import twitch.angelandroidapps.angelstylisedimages.domain.entities.MediaFile;
import twitch.angelandroidapps.angelstylisedimages.presentation.view_managers.ViewManagerControls;
import twitch.angelandroidapps.angelstylisedimages.presentation.view_managers.ViewManagerImage;
import twitch.angelandroidapps.angelstylisedimages.presentation.view_managers.ViewManagerQuality;
import twitch.angelandroidapps.angelstylisedimages.presentation.view_managers.ViewManagerStyleGrid;
import twitch.angelandroidapps.angelstylisedimages.presentation.view_managers.ViewManagerTensor;

/**
 * A placeholder fragmentMain containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private IViewManagerStyleGrid vmStyleGrid;
    private IViewManagerImage vmImage;
    private IViewManagerQuality vmQuality;
    private IViewManagerTensor vmTensor;

    private IViewManagerControls vmControls;
    private boolean gridVisibility;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        vmStyleGrid = new ViewManagerStyleGrid();
        vmStyleGrid.setup(getContext(), this::styleValsUpdated);

        vmImage = new ViewManagerImage();
        vmImage.setup(getContext(), this::bitmapReady);

        vmQuality = new ViewManagerQuality();
        vmQuality.setup(getContext(), this::qualityChanged);

        vmTensor = new ViewManagerTensor();
        vmTensor.setup(getContext(),
                vmStyleGrid.getStyleVals(),
                vmQuality.getQualityIndex(),
                (IViewManagerTensor.IEventCallback) getActivity());

        vmControls = new ViewManagerControls();
        vmControls.setup(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stylised_image, container, false);

        vmStyleGrid.setupView(view);
        vmImage.setupView(view);
        vmTensor.setupView(view);
        vmQuality.setupView(view);

        vmControls.setupView(view);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

        vmTensor.onPause();
    }

    //IMAGE RELATED
    //=============
    @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
    public void doPendingSaveImageIfNeeded(boolean permissionGranted) {

    }


    public void loadImageIfNeeded(Intent intent) {

        if (intent != null) {
            Uri url = intent.getData();
            if (url == null && intent.getExtras() != null) {
                url = (Uri) intent.getExtras().get(Intent.EXTRA_STREAM);
            }
            if (url == null) {
                //check is file type
                ClipData clipData = intent.getClipData();
                url = getFirstUri(clipData);
            }

            loadImageIfNeeded(url);

        }
    }


    private Uri getFirstUri(ClipData clipData) {
        Uri result = null;
        if (clipData != null && clipData.getItemCount() > 0) {
            ClipData.Item item = clipData.getItemAt(0);
            if (item != null) {
                result = item.getUri();
            }
        }
        return result;
    }

    public void loadImageIfNeeded(Uri uri) {
        if (uri != null) {
            vmImage.loadImg(uri);

            //new SetQueryStatisticsGaeAsyncTask().execute(uri.toString());
        }

    }

    public void loadImageIfNeeded(Uri uri, String signature) {
        if (uri != null) {
            vmImage.loadImg(uri, signature);
        }

    }


    public MediaFile saveExternalImage() {
        return vmTensor.saveExternalImg();
    }


    public File saveInternalImg() {
        return vmTensor.saveInternalImg();
    }


    //TENSOR FLOW RELATED
    //===================
    private void styleValsUpdated(float[] styleVals) {
        vmTensor.setStyleVals(styleVals);
    }

    private void bitmapReady(Bitmap bitmap) {
        vmTensor.setBitmap(bitmap);

        //show grid to allow user to change settings
        vmStyleGrid.doExpandGrid();
    }

    private void qualityChanged(int qualityIndex) {
        vmTensor.setQualityIndex(qualityIndex);
    }


    //GRID RELATED
    //=============
    public void setGridAndQualityVisibility(IViewManagerTensor.ImageState hasImage) {
        switch (hasImage) {
            case NoImage -> {
                vmStyleGrid.doExpandGrid();
                vmQuality.setVisibility(true);
            }
            case CreatingImage -> {
                vmStyleGrid.doExpandGrid();
                vmQuality.setVisibility(false);
            }
            case ImageAvailable -> {
                vmStyleGrid.doCollapseGrid();
                vmQuality.setVisibility(true);
            }
        }
    }

    //VIEW MANAGERS
    //=============
    public interface IViewManagerTensor {
        void setup(Context context,
                   float[] initialStyleVals,
                   int qualityIndex,
                   IEventCallback callback);

        void setupView(View view);

        void setBitmap(Bitmap bitmap);

        void setQualityIndex(int qualityIndex);

        void setStyleVals(float[] styleVals);

        void onPause();

        MediaFile saveExternalImg();

        File saveInternalImg();

        public enum ImageState {
            NoImage, CreatingImage, ImageAvailable
        }

        interface IEventCallback {

            void isImageAvailable(ImageState isImageAvailable);
        }
    }

    public interface IViewManagerControls {
        void setup(Context context);

        void setupView(View view);
    }

    public interface IViewManagerQuality {
        void setup(Context context, IEventCallback callback);

        void setupView(View view);

        int getQualityIndex();

        void setVisibility(boolean isVisible);

        interface IEventCallback {
            void onQualityIndexChanged(int qualityIndex);
        }

    }

    public interface IViewManagerImage {
        void setup(Context context, IEventCallback callback);

        void setupView(View view);

        void loadImg(Uri uri);

        void loadImg(Uri uri, String signature);

        @SuppressWarnings("unused")
        Bitmap getBitmap();


        interface IEventCallback {
            void onBitmapReady(Bitmap bitmap);
        }
    }

    public interface IViewManagerStyleGrid {

        void setup(Context context, IEventCallback callback);

        void setupView(View view);

        float[] getStyleVals();

        void doExpandGrid();

        void doCollapseGrid();

        interface IEventCallback {

            void onStyleUpdated(float[] styleVals);
        }
    }
}

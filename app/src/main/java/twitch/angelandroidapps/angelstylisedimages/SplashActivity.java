package twitch.angelandroidapps.angelstylisedimages;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    TextView tvVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        tvVersion = findViewById(R.id.version);

        tvVersion.setText("Version " + BuildConfig.VERSION_NAME);

        //Show for 1.3 seconds, close, then launch MainActivity.
        Intent intent = new Intent(this, MainActivity.class);
        tvVersion.postDelayed(() -> {
            startActivity(intent);
            finish();
        }, 1110);

    }
}

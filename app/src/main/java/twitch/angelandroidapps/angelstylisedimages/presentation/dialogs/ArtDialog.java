package twitch.angelandroidapps.angelstylisedimages.presentation.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import twitch.angelandroidapps.angelstylisedimages.R;
import twitch.angelandroidapps.angelstylisedimages.presentation.adapters.ArtRecyclerAdapter;

/**
 * Created by Angel on 22/9/2017.
 */

public class ArtDialog extends Dialog {

    final RecyclerView rv;

    public ArtDialog(@NonNull Context context, ArtRecyclerAdapter adapter) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_art);

        rv = findViewById(R.id.rv);
        rv.setAdapter(adapter);

        findViewById(R.id.close).setOnClickListener(v->onCloseClicked());
    }

    void onCloseClicked() {
        dismiss();
    }


}

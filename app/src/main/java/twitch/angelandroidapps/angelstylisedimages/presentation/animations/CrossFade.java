package twitch.angelandroidapps.angelstylisedimages.presentation.animations;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/**
 * Created by Angel on 22/9/2017.
 */

public class CrossFade {

    public static void fadeIn(View view, long durationMillis) {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        view.setAlpha(0f);
        view.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        view.animate()
                .alpha(1f)
                .setDuration(durationMillis)
                .setListener(null);
        //for fast release (in case fade out animationEnd is called during fadeIn animation
        view.postDelayed(() -> view.setVisibility(View.VISIBLE), durationMillis);
    }

    public static void fadeOut(View view, long durationMillis) {

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        view.animate()
                .alpha(0f)
                .setDuration(durationMillis)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                        view.setAlpha(1f);
                    }
                });
    }

    @SuppressWarnings("unused")
    public static void animate(View fadeIn, View fadeOut, long durationMillis) {

        fadeIn(fadeIn, durationMillis);

        fadeOut(fadeOut, durationMillis);
    }
}

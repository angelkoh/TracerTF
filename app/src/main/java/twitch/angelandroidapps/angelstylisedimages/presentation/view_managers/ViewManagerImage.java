package twitch.angelandroidapps.angelstylisedimages.presentation.view_managers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.alexvasilkov.gestures.views.GestureFrameLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.signature.ObjectKey;

import twitch.angelandroidapps.angelstylisedimages.MainActivityFragment;
import twitch.angelandroidapps.angelstylisedimages.R;

/**
 * Created by Angel on 21/9/2017.
 */

public class ViewManagerImage implements MainActivityFragment.IViewManagerImage {

    GestureFrameLayout gestureLayout;
     ImageView imagePreview;

    private RequestManager glide;
    private IEventCallback delegate;


    @Override
    public void setup(Context context, IEventCallback callback) {
        glide = Glide.with(context);
        this.delegate = callback;
    }

    @Override
    public void setupView(View view) {

        gestureLayout = view.findViewById(R.id.gesture_layout);
        imagePreview = view.findViewById(R.id.image_preview);

        gestureLayout.getController().getSettings()
                .setRotationEnabled(false)
                .setMaxZoom(10);

    }

    @Override
    public void loadImg(Uri uri) {
        loadImg(uri, "");
    }

    @Override
    public void loadImg(Uri uri, String signature) {

        glide.asBitmap().load(uri).
                apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .signature(new ObjectKey(signature))) // Apply signature conditionally

                .into(new CustomTarget<Bitmap>() {

                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        processBitmap(resource);

                        imagePreview.post(() -> {
                            if (!resource.isRecycled()) {
                                imagePreview.setImageBitmap(resource);
                            }
                        });
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });

    }

    @Override
    public Bitmap getBitmap() {

        Drawable drawable = imagePreview.getDrawable();
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else
            return null;
    }

    private void processBitmap(Bitmap resource) {

        if (delegate != null) {
            delegate.onBitmapReady(resource);
        }
    }
}

package twitch.angelandroidapps.angelstylisedimages.presentation.view_managers;

import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.util.Arrays;

import twitch.angelandroidapps.angelstylisedimages.MainActivityFragment;
import twitch.angelandroidapps.angelstylisedimages.R;
import twitch.angelandroidapps.angelstylisedimages.domain.entities.StyleType;
import twitch.angelandroidapps.angelstylisedimages.presentation.adapters.ArtRecyclerAdapter;
import twitch.angelandroidapps.angelstylisedimages.presentation.dialogs.ArtDialog;

/**
 * Created by Angel on 22/9/2017.
 */

public class ViewManagerControls implements MainActivityFragment.IViewManagerControls {

    private ArtRecyclerAdapter adapter;
    private Context context;
    @Override
    public void setup(Context context ) {

        this.context = context;
        String[] array = context.getResources().getStringArray(R.array.style_array);

        StyleType[] data = Arrays.stream(array).map(StyleType::new).toArray(StyleType[]::new);

        adapter = new ArtRecyclerAdapter(data, context);
    }


    @Override
    public void setupView(View view) {
        view.findViewById(R.id.ib_info).setOnClickListener(v -> onInfoClicked());
    }

    void onInfoClicked() {

        ArtDialog dialog = new ArtDialog(context, adapter);


        Window window = dialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width and height
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(lp);
        }


        dialog.show();
    }
}

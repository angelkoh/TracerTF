package twitch.angelandroidapps.angelstylisedimages.presentation.view_managers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.io.File;
import java.util.Collections;
import java.util.Vector;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;
import twitch.angelandroidapps.angelstylisedimages.MainActivityFragment;
import twitch.angelandroidapps.angelstylisedimages.R;
import twitch.angelandroidapps.angelstylisedimages.domain.entities.MediaFile;
import twitch.angelandroidapps.angelstylisedimages.domain.handlers.FileHandler;
import twitch.angelandroidapps.angelstylisedimages.domain.utils.ImageUtils;
import twitch.angelandroidapps.angelstylisedimages.presentation.animations.CrossFade;
import twitch.angelandroidapps.angelstylisedimages.presentation.views.OverlayView;

/**
 * Created by Angel on 21/9/2017.
 */

public class ViewManagerTensor implements MainActivityFragment.IViewManagerTensor {
    //debug related
    private static final boolean DEBUG = false;

    private static final String MODEL_FILE = "file:///android_asset/stylize_quantized.pb";
    private static final String TAG = "Angel: Tensor VM";
    private static final int[] SIZES = {128, 192, 256, 384, 512, 720};
    private static final String INPUT_NODE = "input";
    private static final String STYLE_NODE = "style_num";
    private static final String OUTPUT_NODE = "transformer/expand/conv3/conv/Sigmoid";
    private static final int NUM_STYLES = ViewManagerStyleGrid.NUM_STYLES;

    ProgressBar progressBar;
    ImageView imageTexture;
    ImageView btnSwap; //show original photo
    private Bitmap imageBitmap = null;
    private Bitmap croppedBitmap;
    private Bitmap cropCopyBitmap;
    private Bitmap textureCopyBitmap;
    private long lastProcessingTimeMs;

    private DisposableCompletableObserver imageSubscription;

    private int previewWidth = 0;
    private int previewHeight = 0;
    private Matrix frameToCropTransform;
    private int[] intValues;     //pixels
    private float[] floatValues; //inputTo/outputFrom tensorflow (separated to RGB components)
    private TensorFlowInferenceInterface inferenceInterface = null;

    private float[] styleVals = new float[ViewManagerStyleGrid.NUM_STYLES];

    // Start at a medium size, but let the user step up through smaller sizes so they don't get
    // immediately stuck processing a large image.
    private int desiredSize = 128;
    private int initializedSize = 0;

    private IEventCallback delegate;
    private Context context;


    //CREATION & SETUP
    //================
    @Override
    public void setup(Context context, float[] styleVals, int qualityIndex, IEventCallback callback) {
        this.context = context;
        initializeTensorInference(context);

        setQualityIndex(qualityIndex);
        setStyleVals(styleVals);

        this.delegate = callback;

    }

    private void initializeTensorInference(Context context) {
        if (inferenceInterface != null) {
            inferenceInterface.close();
        }
        inferenceInterface = new TensorFlowInferenceInterface(context.getAssets(), MODEL_FILE);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void setupView(View view) {
        progressBar = view.findViewById(R.id.progress_bar);
        imageTexture = view.findViewById(R.id.image_texture);
        btnSwap = view.findViewById(R.id.btn_swap);

        btnSwap.setOnTouchListener(this::onSwapPressed);

//        addCallback(this::renderDebug);
//        processTensor();
    }

    @Override
    public void onPause() {
        disposeSubscription();
    }


    //SHOW AND HIDE TEXTURE IMAGE
    //===========================
    boolean onSwapPressed(@SuppressWarnings("UnusedParameters") View view, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                CrossFade.fadeOut(imageTexture, 200);

                return true;
            case MotionEvent.ACTION_UP:
                CrossFade.fadeIn(imageTexture, 300);

                return true;
        }

        return false;
    }

    //TENSOR FLOW FUNCTIONS
    //=====================
    private void processTensor() {
        try {
            processBitmap();
        } catch (Exception e) {
            print("Exception encountered! " + e.getMessage());
        }
    }

    private void processBitmap() {
        if (imageBitmap != null && !imageBitmap.isRecycled()) {
            showProgressBar(); // Show progress bar while processing

            textureCopyBitmap = null;

            previewWidth = imageBitmap.getWidth();
            previewHeight = imageBitmap.getHeight();

            if (desiredSize != initializedSize) {
                resetPreviewBuffers(); // Reset preview buffers if the desired size has changed
            }

            // Calculate the cropping area to center the crop, considering the shorter side
            float scale = Math.max((float) desiredSize / previewWidth, (float) desiredSize / previewHeight);
            float width = scale * previewWidth;
            float height = scale * previewHeight;
            float dx = (desiredSize - width) / 2;
            float dy = (desiredSize - height) / 2;

            // Apply the transformation to crop the bitmap to the center
            frameToCropTransform.setScale(scale, scale);
            frameToCropTransform.postTranslate(dx, dy);

            // Draw the centered cropped bitmap onto the canvas
            final Canvas canvas = new Canvas(croppedBitmap);
            canvas.drawBitmap(imageBitmap, frameToCropTransform, null);

            // Hide the view before running the tensor processing
            imageTexture.setImageResource(android.R.color.transparent);
            btnSwap.setVisibility(View.GONE);

            createImageSubscription();
            Completable completable = Completable.fromAction(() -> {

                        // Create a copy of the cropped bitmap for further processing
                        cropCopyBitmap = Bitmap.createBitmap(croppedBitmap);

                        final long startTime = SystemClock.uptimeMillis();
                        stylizeImage(croppedBitmap); // Run the image stylization process
                        lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;

                        textureCopyBitmap = Bitmap.createBitmap(croppedBitmap); // Store the processed bitmap

                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

            completable.subscribe(imageSubscription); // Start processing asynchronously
        } else {
            hideProgressBar(); // Hide progress bar if the image is not valid
        }
    }


    //RX SUBSCRIPTIONS
    //================
    private void createImageSubscription() {
        disposeSubscription();
        imageSubscription = new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                print("completed in " + lastProcessingTimeMs / 1000 + "." + lastProcessingTimeMs % 1000 + " sec");

                requestRender();
                if (textureCopyBitmap != null && !textureCopyBitmap.isRecycled()) {
                    //  if (croppedBitmap != null && !croppedBitmap.isRecycled()) {
                    imageTexture.setImageBitmap(textureCopyBitmap);
                }

                CrossFade.fadeIn(imageTexture, 1000);
                btnSwap.setVisibility(View.VISIBLE);

                hideProgressBar();

                if (delegate != null) {
                    delegate.isImageAvailable(ImageState.ImageAvailable);
                }
            }

            @Override
            public void onError(Throwable e) {
                print("Error");
                hideProgressBar();

                imageTexture.setImageResource(android.R.color.transparent);
                btnSwap.setVisibility(View.GONE);

                if (delegate != null) {
                    delegate.isImageAvailable(ImageState.NoImage);
                }
            }
        };
    }

    private void disposeSubscription() {
        if (imageSubscription != null && !imageSubscription.isDisposed()) {
            imageSubscription.dispose();
        }
    }

    private void stylizeImage(final Bitmap bitmap) {
        // Extract pixel values from the bitmap into an integer array
        bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

        // Convert pixel values from ARGB to normalized RGB values
        for (int i = 0; i < intValues.length; ++i) {
            final int val = intValues[i];
            floatValues[i * 3] = ((val >> 16) & 0xFF) / 255.0f;     // Red
            floatValues[i * 3 + 1] = ((val >> 8) & 0xFF) / 255.0f;  // Green
            floatValues[i * 3 + 2] = (val & 0xFF) / 255.0f;         // Blue
        }

        // Log input dimensions for debugging
        print(String.format("Stylize Width: %s , Height: %s", bitmap.getWidth(), bitmap.getHeight()));

        // Feed input data into TensorFlow model
        inferenceInterface.feed(INPUT_NODE, floatValues, 1, bitmap.getWidth(), bitmap.getHeight(), 3);
        inferenceInterface.feed(STYLE_NODE, styleVals, NUM_STYLES);

        // Run inference
        inferenceInterface.run(new String[]{OUTPUT_NODE}, isDebug());

        // Fetch output data from TensorFlow model
        inferenceInterface.fetch(OUTPUT_NODE, floatValues);

        // Convert processed output data back to ARGB pixel values
        for (int i = 0; i < intValues.length; ++i) {
            intValues[i] =
                    0xFF000000                                       // Alpha
                            | (((int) (floatValues[i * 3] * 255)) << 16)  // Red
                            | (((int) (floatValues[i * 3 + 1] * 255)) << 8) // Green
                            | ((int) (floatValues[i * 3 + 2] * 255));      // Blue
        }

        // Set the processed pixels back to the bitmap
        bitmap.setPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
    }

    private void resetPreviewBuffers() {
        recycleBitmap(croppedBitmap);
        croppedBitmap = Bitmap.createBitmap(desiredSize, desiredSize, Bitmap.Config.ARGB_8888);

        frameToCropTransform = ImageUtils.getTransformationMatrix(
                previewWidth, previewHeight,
                desiredSize, desiredSize,
                0, true);
        //
        //        Matrix cropToFrameTransform = new Matrix();
        //        frameToCropTransform.invert(cropToFrameTransform);


        intValues = new int[desiredSize * desiredSize];

        floatValues = new float[desiredSize * desiredSize * 3]; //RGB values
        initializedSize = desiredSize;
    }

    private void recycleBitmap(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
        }
    }


    //IMAGE VIEW
    //==========
    private void requestRender() {
        //imageTensor.postInvalidate();
    }

    @SuppressWarnings("unused")
    public void addCallback(final OverlayView.DrawCallback callback) {
//        imageTensor.addCallback(callback);
    }

    @SuppressWarnings("unused")
    private void renderDebug(Canvas canvas) {
        final Bitmap texture = textureCopyBitmap;
        if (texture != null) {
            final Matrix matrix = new Matrix();
            final float scaleFactor =
                    Math.min(
                            (float) canvas.getWidth() / texture.getWidth(),
                            (float) canvas.getHeight() / texture.getHeight());
            matrix.postScale(scaleFactor, scaleFactor);
            canvas.drawBitmap(texture, matrix, new Paint());
        }

        if (!isDebug()) {
            return;
        }

        final Bitmap copy = cropCopyBitmap;
        if (copy == null) {
            return;
        }

        canvas.drawColor(0x55000000);

        final Matrix matrix = new Matrix();
        final float scaleFactor = 2;
        matrix.postScale(scaleFactor, scaleFactor);
        matrix.postTranslate(
                canvas.getWidth() - copy.getWidth() * scaleFactor,
                canvas.getHeight() - copy.getHeight() * scaleFactor);
        canvas.drawBitmap(copy, matrix, new Paint());

        final Vector<String> lines = new Vector<>();

        final String[] statLines = inferenceInterface.getStatString().split("\n");
        Collections.addAll(lines, statLines);

        lines.add("");

        lines.add("Frame: " + previewWidth + "x" + previewHeight);
        lines.add("Crop: " + copy.getWidth() + "x" + copy.getHeight());
        lines.add("View: " + canvas.getWidth() + "x" + canvas.getHeight());
        lines.add("Rotation: " + 0);
        lines.add("Inference time: " + lastProcessingTimeMs + "ms");
        lines.add("Desired size: " + desiredSize);
        lines.add("Initialized size: " + initializedSize);

        //   borderedText.drawLines(canvas, 10, canvas.getHeight() - 10, lines);

        lines.forEach(this::print);

    }

    //PROGRESS BAR
    //============
    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private void showProgressBar() {
        if (delegate != null) {
            delegate.isImageAvailable(ImageState.CreatingImage);
        }
        progressBar.setVisibility(View.VISIBLE);
    }

    //CALLBACKS
    //=========
    @Override
    public void setBitmap(Bitmap bitmap) {
        this.imageBitmap = bitmap;

        if (bitmap != null && !bitmap.isRecycled()) {
            String s = "setBitmap() Width: " + bitmap.getWidth() + ", Height: " + bitmap.getHeight();
            print(s);

            processTensor();
        }
    }

    @Override
    public MediaFile saveExternalImg() {
        if (textureCopyBitmap != null && !textureCopyBitmap.isRecycled()) {

            Context context = progressBar.getContext().getApplicationContext();
            return FileHandler.INSTANCE.saveExternal(context,
                    textureCopyBitmap, "");


            // return ImageUtils.saveBitmapWithCurrentTime(textureCopyBitmap);
        } else {
            return null;
        }
    }

    @Override
    public File saveInternalImg() {
        if (textureCopyBitmap != null && !textureCopyBitmap.isRecycled()) {

            return ImageUtils.saveBitmapInternal(context, textureCopyBitmap);
        } else {
            return null;
        }
    }

    @Override
    public void setStyleVals(float[] styleVals) {
        this.styleVals = styleVals;
        processTensor();

    }


    @Override
    public void setQualityIndex(int qualityIndex) {
        if (qualityIndex < 0) {
            qualityIndex = 0;
        } else if (qualityIndex >= SIZES.length) {
            qualityIndex = SIZES.length - 1;
        }
        desiredSize = SIZES[qualityIndex];

        processTensor();
    }

    //DEBUGS
    //======
    private boolean isDebug() {
        return DEBUG;
    }

    private void print(String s) {
        Log.d(TAG, s);
    }
}

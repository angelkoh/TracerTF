package twitch.angelandroidapps.angelstylisedimages.presentation.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import twitch.angelandroidapps.angelstylisedimages.R;
import twitch.angelandroidapps.angelstylisedimages.domain.entities.StyleType;

/**
 * Created by Angel on 22/9/2017.
 */

public class ArtRecyclerAdapter extends RecyclerView.Adapter<ArtRecyclerAdapter.ArtViewHolder> {

    private final RequestManager glide;
    private final StyleType[] data;

    public ArtRecyclerAdapter(StyleType[] data, Context context) {
        this.data = data;

        glide = Glide.with(context);
    }

    @NonNull
    @Override
    public ArtViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dialog_art_item, parent, false);

        return new ArtViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ArtViewHolder holder, int position) {

        holder.bind(data[position]);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class ArtViewHolder extends RecyclerView.ViewHolder {

        final ImageView artImage;
        final TextView artName;
        final TextView artist;

        public ArtViewHolder(View itemView) {
            super(itemView);
            artImage = itemView.findViewById(R.id.art_image);
            artName = itemView.findViewById(R.id.art_name);
            artist = itemView.findViewById(R.id.artist);
        }

        public void bind(StyleType styleType) {

            glide.load(Uri.parse("file:///android_asset/thumbnails/style" + styleType.getStyle() + ".jpg"))
                    .into(artImage);

            artName.setText(styleType.getTitleWithYear());
            artist.setText(styleType.getArtist());
        }
    }
}

package twitch.angelandroidapps.angelstylisedimages.presentation.dialogs;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.core.content.FileProvider;

import java.io.File;

import twitch.angelandroidapps.angelstylisedimages.R;


public class ShareImageDialog {

    public static void openDialog(Context context,
                                  File file) {

        if (file != null && file.exists()) {

            String shareText =
                    prepareShareText(context);

            // Uri uri = Uri.fromFile(file);
            Uri uri = FileProvider.getUriForFile(context,
                    context.getString(R.string.file_provider_authority),
                    file);

            final Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_STREAM, uri);

            intent.putExtra(Intent.EXTRA_TEXT, shareText);
            context.startActivity(Intent.createChooser(intent, "Share media"));

        }
    }

    private static String prepareShareText(Context context) {

        return "Shared via " + context.getString(R.string.app_name) + " app";

    }
}

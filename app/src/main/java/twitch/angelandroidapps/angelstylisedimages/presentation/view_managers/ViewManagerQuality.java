package twitch.angelandroidapps.angelstylisedimages.presentation.view_managers;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import twitch.angelandroidapps.angelstylisedimages.MainActivityFragment;
import twitch.angelandroidapps.angelstylisedimages.R;
import twitch.angelandroidapps.angelstylisedimages.domain.handlers.PreferenceHandler;

/**
 * Created by Angel on 21/9/2017.
 */

public class ViewManagerQuality implements MainActivityFragment.IViewManagerQuality {

    private static final long DEBOUNCE_DELAY_MS = 300; // Debounce delay
    private final Handler handler = new Handler();
    private Runnable debounceRunnable;

    SeekBar sbQuality;
    private IEventCallback delegate;

    private int currentProgress = 0;
    private Context context;

    private ViewGroup llQuality;

    @Override
    public void setup(Context context, IEventCallback callback) {
        this.delegate = callback;
        this.context = context;
        currentProgress = PreferenceHandler.getQualityIndex(context);
    }


    @Override
    public void setupView(View view) {
        llQuality = view.findViewById(R.id.ll_quality);
        sbQuality = view.findViewById(R.id.sb_quality);

        sbQuality.setProgress(currentProgress);
        sbQuality.setOnSeekBarChangeListener(new SimpleSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (debounceRunnable != null) {
                    handler.removeCallbacks(debounceRunnable);
                }

                debounceRunnable = () -> {
                    if (currentProgress != progress) {
                        currentProgress = progress;
                        if (delegate != null) {
                            delegate.onQualityIndexChanged(progress);
                        }
                        PreferenceHandler.setQualityIndex(context, progress);
                    }
                };
                handler.postDelayed(debounceRunnable, DEBOUNCE_DELAY_MS);
            }
        });

    }

    @Override
    public int getQualityIndex() {
        return currentProgress;
    }

    @Override
    public void setVisibility(boolean isVisible) {
        llQuality.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    private abstract static class SimpleSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {


        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }
}

package twitch.angelandroidapps.ads;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import java.util.Arrays;
import java.util.List;

import twitch.angelandroidapps.angelstylisedimages.R;


public class ApplicationAdViewAdMob3 {

    private static final String REALME = "A4C7F45FA0B2018F60947BB4B5D85774";
    private static final String REALME_R = "E4A264C1346C19D73635433A5539113F";
    private static final String TAG = "Angel: amob";
    final OnAdClosedListener adClosedListener;
    private final List<String> TEST_DEVICES = Arrays.asList(
            AdRequest.DEVICE_ID_EMULATOR, REALME, REALME_R
    );
    private AdRequest adRequest = null;
    private InterstitialAd interstitialAd = null;
    public ApplicationAdViewAdMob3(Context context, OnAdClosedListener adClosedListener) {

        this.adClosedListener = adClosedListener;

        MobileAds.initialize(context,
                initializationStatus -> requestNewInterstitial(context));


    }

    public void loadAdaptiveAd(ViewGroup view, int adUnitId) {


        requestNewInterstitial(view.getContext());
        adRequest = initAdRequest();
        view.post(() -> {
            Context context = view.getContext();
            AdView adView = new AdView(context);
            adView.setAdUnitId(context.getString(adUnitId));
            int viewWidth = pxToDp(context, view.getWidth());
            adView.setAdSize(
                    AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(
                            context, viewWidth)
            );
            view.addView(adView);
            adView.loadAd(adRequest);

            print("amob w = " + view.getWidth() + "/" + viewWidth + ", viewWidth, h = " + view.getHeight());

        });
    }

    public boolean isInterstitialLoaded() {
        return interstitialAd != null;
    }

    public void showInterstitial(Activity activity) {
        if (interstitialAd != null) {
            interstitialAd.show(activity);
        }
    }

    public void showInterstitialIfAvailable(Activity activity) {
        if (interstitialAd != null) {
            interstitialAd.show(activity);
        } else {
            requestNewInterstitial(activity);
        }
    }

    private void requestNewInterstitial(Context context) {
        adRequest = initAdRequest();
        interstitialAd = null;
        if (adClosedListener != null) {
            InterstitialAd.load(context,
                    context.getString(R.string.admob_ad_unit_id_interstitial),
                    adRequest,
                    new InterstitialAdLoadCallback() {
                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            interstitialAd = null;
                        }

                        @Override
                        public void onAdLoaded(@NonNull InterstitialAd ad) {
                            interstitialAd = ad;
                            interstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                                @Override
                                public void onAdDismissedFullScreenContent() {
                                    adClosedListener.onAdClosed();
                                    requestNewInterstitial(context);
                                }

                                @Override
                                public void onAdShowedFullScreenContent() {
                                    interstitialAd = null;
                                }
                            });
                        }
                    });

        }
    }

    private AdRequest initAdRequest() {
        MobileAds.setAppVolume(0f);
        MobileAds.setAppMuted(true);
        RequestConfiguration requestConfiguration = new RequestConfiguration.Builder()
                .setTestDeviceIds(TEST_DEVICES)
                .build();
        MobileAds.setRequestConfiguration(requestConfiguration);

        return new AdRequest.Builder().build();
    }

    private int pxToDp(Context context, int px) {
        DisplayMetrics outMetrics = context.getResources().getDisplayMetrics();
        float density = outMetrics.density;
        return (int) (px / density);
    }

    private void print(String s) {
        Log.d(TAG, s);
    }

    public interface OnAdClosedListener {
        void onAdClosed();
    }
}

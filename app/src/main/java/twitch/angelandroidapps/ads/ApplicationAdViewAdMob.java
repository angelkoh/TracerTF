package twitch.angelandroidapps.ads;
//
//
//import android.content.Context;
//
//import com.google.android.gms.ads.AdListener;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.interstitial.InterstitialAd;
//
//import twitch.angelandroidapps.angelstylisedimages.R;
//
//
//@SuppressWarnings("unused")
//public class ApplicationAdViewAdMob {
//
//    private static final String TEST_DEVICE = "E626D874AF5FB68E9E2AB70E7B8DB3BA";
//    private AdView adView = null;
//    private InterstitialAd interstitialAd = null;
//
//    public ApplicationAdViewAdMob(Context context,
//                                  AdView adView,
//                                  IAppAdListener adListenerDelegate) {
//
//        //noinspection EmptyCatchBlock
//        try {
//            AdRequest adRequest = new AdRequest.Builder()
//                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                    .addTestDevice(TEST_DEVICE)
//                    .build();
//            this.adView = adView;
//            if (adView != null) {
//                adView.loadAd(adRequest);
//            }
//
//            if (adListenerDelegate != null) {
//                interstitialAd = new InterstitialAd(context);
//
//                interstitialAd.setAdUnitId(context.getString(R.string.admob_ad_unit_id_interstitial));
//                requestNewInterstitial();
//                interstitialAd.setAdListener(new InterstitialListener(adListenerDelegate));
//            }
//        } catch (Exception e) {
//
//        }
//    }
//
//
//    //INTERSTITIAL RELATED
//    //====================
//    public boolean isInterstitialLoaded() {
//        return (interstitialAd != null) && interstitialAd.isLoaded();
//    }
//
//    public void showInterstitial() {
//
//        if (interstitialAd != null) {
//            interstitialAd.show();
//        }
//    }
//
//    private void requestNewInterstitial() {
//
//        if (interstitialAd != null) {
//            AdRequest adRequest = new AdRequest.Builder()
//                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                    .addTestDevice(TEST_DEVICE)
//                    .build();
//            interstitialAd.loadAd(adRequest);
//        }
//    }
//
//    //AD VISIBILITY
//    //==============
//    public void setVisibility(int visibility) {
//        adView.setVisibility(visibility);
//    }
//
//    //INTERFACE
//    //=========
//    public interface IAppAdListener {
//
//        void onInterstitialAdClosed();
//    }
//
//    //LISTENERS
//    //=========
//    private class InterstitialListener extends AdListener {
//        private final IAppAdListener delegate;
//
//        public InterstitialListener(IAppAdListener adListenerDelegate) {
//            this.delegate = adListenerDelegate;
//        }
//
//        @Override
//        public void onAdClosed() {
//            requestNewInterstitial();
//            delegate.onInterstitialAdClosed();
//        }
//
//        @Override
//        public void onAdFailedToLoad(int i) {
//            //super.onAdFailedToLoad(i);
//            //do nothing
//        }
//    }
//
//}

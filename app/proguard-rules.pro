# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Apps\Programming\sdk\android_sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

## SETTINGS AND PREFERENCE
-repackageclasses ''
-renamesourcefileattribute SourceFile
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod

## jxandroid



## MISCELLANEOUS
-dontwarn com.innahema.collections.query.queriables.**
-dontwarn com.sun.jna.**
-dontwarn java.lang.FunctionalInterface
-dontwarn java.lang.invoke.*
-dontwarn org.jetbrains.annotations.*
-dontwarn sun.misc.Unsafe

##HEADLESS WEBVIEW
-keepattributes SetJavaScriptEnabled
-keepattributes JavascriptInterface
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}
-keepclassmembers class **.*JavascriptInterface {
    *;
}
-keepclassmembers class **.*MyJavaScriptInterface {
    *;
}
-keep public class **.*JavascriptInterface
-keep public class **.*MyJavaScriptInterface

## GOOGLE PLAY SERVICES
-dontwarn com.google.android.gms.**
-keep public class com.google.android.gms.* { public *; }

## Support for Google Play Services
## http://developer.android.com/google/play-services/setup.html
-keep class * extends java.util.ListResourceBundle { protected Object[][] getContents();}
-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable { public static final *** NULL;}
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * { @com.google.android.gms.common.annotation.KeepName *;}
-keepnames class * implements android.os.Parcelable { public static final ** CREATOR;}

## Glide Image handler
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

## JSOUP
#-keep public class org.jsoup.** { public *; }
-keeppackagenames org.jsoup.nodes
##-keep class twitch.angelandroidapps.tracerlightbox.data.urlImages.** { *; }

## Retrofit 2.X
## https://square.github.io/retrofit/ ##
-dontwarn okio.**
-dontwarn retrofit2.**
-dontwarn retrofit2.Platform$Java8
-keep class retrofit2.** { *; }
-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

## RXJAVA
## https://github.com/ReactiveX/RxJava/issues/3097 ##
-keep class rx.internal.util.unsafe.** { *; }




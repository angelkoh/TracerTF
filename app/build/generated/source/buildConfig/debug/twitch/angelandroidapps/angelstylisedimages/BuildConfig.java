/**
 * Automatically generated file. DO NOT MODIFY
 */
package twitch.angelandroidapps.angelstylisedimages;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "twitch.angelandroidapps.angelstylisedimages";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 350100;
  public static final String VERSION_NAME = "1.1";
}
